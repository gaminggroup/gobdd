package assert

import (
	"bitbucket.org/gaminggroup/gobdd/types"
	"bitbucket.org/gaminggroup/gobdd/util"
	"fmt"
)

func ToBeNil() types.Matcher {
	return &NilMatcher{}
}

type NilMatcher struct {
	types.Matcher
}

func (m *NilMatcher) Match(actual interface{}) (bool, error) {
	return actual == nil, nil
}

func (m *NilMatcher) Message(actual interface{}) string {
	return fmt.Sprintf("%s should be nil", util.Pretty(actual))
}
